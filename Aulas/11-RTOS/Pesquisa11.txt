Pesquisa 11 - Sabrina

1) A task de um RTOS � como uma fun��o de um c�digo. Ela diz o que deve ser feito. Contudo, no rtos, as tasks tem prioridades diferentes, entao se o sistema chamar duas ao mesmo tempo, ele tem que saber lidar com elas e qual vai acontecer antes, se vai dividir o processamento entre ambas, etc.

2) Um exemplo de hard rtos � o lan�amento de m�sseis teleguiados. Eles devem ter uma hora de lan�amento extremamente precisa para chegar no local certo, pois uma diferen�a de 5 minutos pode mudar metros ou at� quil�metros da dist�ncia original.
Um sistema soft �, por exemplo, um sistema de audio em uma sala de estar. Se o audio falhar uma vez, n�o tem problema, nada vai quebrar. Contudo, se o audio ficar falhando repetidadamente, o usu�rio vai trocar de aparelho, pois n�o � �til um sound system com atrasos constantes.

3)
- uKOS (GNU GPL)
- BRTOS (MIT)
- Distortos (Mozilla)
...

4) O rtos precisa lidar imediatamente com a interrup��o, pois ela interrompe o escalonamento do sistema para usar processamento exclusivo. J� no os convencional, uma tarefa, mesmo que com maior prioridade, n�o define a quantidade de processamento requerida.

5) Tarefas no estado blocked n�o est�o realizando processamento, ou seja, outras atrefas podem ser executadas enquanto isso. Normalmente as tarefas ficam bloqueadas por um timeout ou um semaforo, e quando este � disparado, elas consomem o semafaro e executam, saindo do estado blocked. 

6) O escalonador do freeRTOS determina que tasks v�o ser executadas a cada TICK. O interessante dele � que, quando nenhuma task est� sendo executada, ele roda uma task espec�fica chamada IDLE, que basicamente dorme, economizando energia e processamento.
"O escalonador � a parte do RTOS respons�vel por decidir qual tarefa ir� executar
e por quanto tempo ela ter� acesso ao processador" - Por exemplo, existe uma task chamada vControlTask, que determina o tempo correto pra come�ar um novo ciclo de controle, e existe a vKeyHandlerTask, uma outra task do freeRTOS que checa se uma tecla foi apertada. O escalonador vai ser o "scrum master" dessas 3 tarefas (control, handler e idle) e falar quando cada uma vai ser acionada.