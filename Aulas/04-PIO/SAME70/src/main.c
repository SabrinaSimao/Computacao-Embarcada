/**
 * 5 semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

#define LED_PIO
#define LED_PIO PIOC
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

#define BUT_PIO
#define BUT_PIO PIOA
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)



/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/

void _pio_set(Pio *p_pio, const uint32_t ul_mask) {

	p_pio->PIO_SODR = ul_mask;
}

void _pio_clear(Pio *p_pio, const uint32_t ul_mask){
	
	
	p_pio->PIO_CODR = ul_mask;
}

void _pio_pull_up(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_pull_up_enable){

	if(ul_pull_up_enable == 0){
		p_pio->PIO_PUDR = ul_mask;
	}else{
		p_pio->PIO_PUER = ul_mask;
	}

}

void _pio_set_output(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_default_level,
const uint32_t ul_multidrive_enable, const uint32_t ul_pull_up_enable){
	p_pio->PIO_OER = ul_mask;
	p_pio->PIO_PER = ul_mask;
	
	_pio_pull_up(p_pio, ul_mask, ul_pull_up_enable);
	
	
	if(ul_multidrive_enable){
		p_pio->PIO_MDER = ul_mask;
	}else{
		p_pio->PIO_MDDR = ul_mask;
	}

	if(ul_default_level){
		_pio_set(p_pio, ul_mask);
	}else{
		_pio_clear(p_pio, ul_mask);
	}
	
	
}

void BUT_init(void){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(BUT_PIO_ID);
	pio_set_input(BUT_PIO, BUT_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup??o */
	pio_enable_interrupt(BUT_PIO, BUT_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIN_MASK, PIO_IT_FALL_EDGE, Button0_Handler);

	/* habilita interrup?c?o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 1);
};


/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Funcao principal chamada na inicalizacao do uC.
int main(void){
	
	//set clock
	sysclk_init();
	
	//Desliga reinicializa��o indesejada
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	//Ligar o PIOC que vai controlar o LED
	
	//pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(BUT_PIO_ID);
	
	_pio_set_output(LED_PIO, LED_PIO_PIN_MASK, 0, 0, 0);
	
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_DEFAULT);
	_pio_pull_up(BUT_PIO, BUT_PIO_PIN_MASK, 1);

	
	// super loop
	// aplicacoes embarcadas n�o devem sair do while(1).
	while (1) {

	int value = pio_get(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK);
		if( value == 0){
			for(int i = 0; i<5; i++){
					pio_set(PIOC, LED_PIO_PIN_MASK);
					delay_ms(200);
					_pio_clear(PIOC, LED_PIO_PIN_MASK);
					delay_ms(200);
				}
			}else{
				_pio_set(PIOC, LED_PIO_PIN_MASK);
		}

	}
	return 0;
}
