/**
 * 5 semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

#define LED_PIO
#define LED_PIO PIOC
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

#define BUT_PIO
#define BUT_PIO PIOA
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)



/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/

/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Funcao principal chamada na inicalizacao do uC.
int main(void){
	
	//set clock
	sysclk_init();
	
	//Desliga reinicializa��o indesejada
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	//Ligar o PIOC que vai controlar o LED
	
	//pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(BUT_PIO_ID);
	
	pio_configure(PIOC, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);

	
	// super loop
	// aplicacoes embarcadas n�o devem sair do while(1).
	while (1) {

	int value = pio_get(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK);
		if( value == 0){
			for(int i = 0; i<5; i++){
					pio_set(PIOC, LED_PIO_PIN_MASK);
					delay_ms(200);
					pio_clear(PIOC, LED_PIO_PIN_MASK);
					delay_ms(200);
				}
			}else{
				pio_set(PIOC, LED_PIO_PIN_MASK);
		}

	}
	return 0;
}
